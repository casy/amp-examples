var uart = require('uart');

function ArrayToString(fileData) {
  var dataString = "";
  for (var i = 0; i < fileData.length; i++) {
    dataString += String.fromCharCode(fileData[i]);
  }
  return dataString;
}

var msgbuf = [97, 98, 99, 100];

// user uart
var serial = uart.open({
  id: 'UART2'
});

// user uart send data
serial.write(msgbuf);

// user uart data receive 
serial.on('data', function(data) {
  console.log('uart receive data is ' + ArrayToString(data));
});