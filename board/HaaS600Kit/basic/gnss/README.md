### GNSS示例
* 配置
    ```
    "io": {
    "UART1": {
      "type": "UART",
      "port": 1,
      "dataWidth": 8,
      "baudRate": 9600,
      "stopBits": 1,
      "flowControl": "cts",
      "parity": "none"
    }
  }

    ```

* 应用

  1、创建UART实例，port为1，对应的HaaS600上已连接GNSS模块；
	2、运行用例，可以看到GNSS模组定时上报定位数据；