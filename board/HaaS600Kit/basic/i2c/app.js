var i2c = require('i2c');

var lm75 = i2c.open({
	id: 'I2C0',
	success: function () {
		console.log('lm75 sensor open success')
	},
	fail: function () {
		console.log('lm75 sensor open failed')
	}
});

function lm75tmpGet() {
	var temp;
	var sig = 1;
	var regval = lm75.readMem(0x00, 2);
	console.log('read regval is ' + regval);
	var tempAll = (regval[0] << 8) + regval[1];
	console.log('tempAll is ' + tempAll);

	if (regval[0] & 0x80 != 0) {
		tempAll = ~(tempAll) + 1;
		sig = -1;
	}

	tempAll = tempAll >> 5;
	console.log('tempAll final data ' + tempAll);
	temp = tempAll * 0.125 * sig;

	return temp;
}

setInterval(function () {
	temp = lm75tmpGet();
	console.log('lm75 data is ' + temp);
}, 1000);