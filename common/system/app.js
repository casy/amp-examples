/* system call */
console.log('system version is: ' + system.version());            /* print system version */
console.log('module version is: ' + system.versions().module);    /* print module version */
console.log('system platform is: ' + system.platform());          /* print system platform */
console.log('system uptime is: ' + system.uptime());              /* print system time */
console.log('system heapTotal is: ' + system.memory().heapTotal); /* print system total heap memory value */
console.log('system heapUsed is: ' + system.memory().heapUsed);   /* print system used heap memory value */

/* process call */
console.log('system version is: ' + process.version());            /* print system version */
console.log('module version is: ' + process.versions().module);    /* print module version */
console.log('system platform is: ' + process.platform());          /* print system platform */
console.log('system uptime is: ' + process.uptime());              /* print system time */
console.log('system heapTotal is: ' + process.memory().heapTotal); /* print system total heap memory value */
console.log('system heapUsed is: ' + process.memory().heapUsed);   /* print system used heap memory value */