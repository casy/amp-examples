var audioplayer = require('audioplayer');

var audioplayerState = ['stop', 'paused', 'playing', 'listplay_begin', 'listplay_end', 'error'];

audioplayer.on('stateChange', function (state) {
  console.log('audioplayer state: ' + audioplayerState[state]);
});

audioplayer.setVolume(60);

audioplayer.play("/resources/test.mp3");

setTimeout(function () {
  console.log("playback pause");
  audioplayer.pause();
  var position = audioplayer.getPosition();
  var duration = audioplayer.getDuration();
  console.log('playback progress: ' + position + '/' + duration);
}, 5000)

setTimeout(function () {
  console.log("playback resume");
  audioplayer.resume();
}, 10000)

setTimeout(function () {
  console.log("playback from 1s");
  audioplayer.seekto(1);
}, 15000)

setTimeout(function () {
  var position = audioplayer.getPosition();
  var duration = audioplayer.getDuration();
  console.log('playback progress: ' + position + '/' + duration);
}, 16000)

setTimeout(function () {
  console.log("playback stop");
  audioplayer.stop();
}, 30000)