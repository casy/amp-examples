var kv = require('kv');

var key = 'key-test';
var value = 'this is amp kv test file';

// write key-value
kv.setStorageSync(key, value);

// get key-value
var val = kv.getStorageSync(key);
console.log('kv read: ' + val);

// remove key-value
kv.removeStorageSync(key);